# Mohr_Circle

Ce d�pot contient des scriptes illustrant la construction du celcre de Morh et du tri-cercle de Mohr.

Python 3.x :

	Mohr.py    => construction du cercle de mohr simple

	TriMohr.py => construction du tri-cercle de Morh

NoteBook Jupyther :

	Tri Morh Circle.ipynb      => construction du tri-cercle de Morh

	Tri Morh Circle-Anim.ipynb => construction du tri-cercle de Morh (version avec animation)

